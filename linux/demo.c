#define _GNU_SOURCE
#include <stdio.h>      // standard input / output functions
#include <stdlib.h>
#include <string.h>     // string function definitions
#include <unistd.h>     // UNIX standard function definitions
#include <fcntl.h>      // File control definitions
#include <errno.h>      // Error number definitions
#include <termios.h>    // POSIX terminal control definitions
#include <pthread.h>
#include <stdint.h>

#include "capoui.h"


void handle_buttons(const uint8_t *data, uint8_t data_length)
{
	if (data_length != 1)
	{
		fprintf(stderr, "ERROR: Buttons peripheral communicate data wrong length (%u), should be 1 byte\n", data_length);
		return;
	}

	switch (data[0])
	{
		case CAPOUI_BUTTON_S:
			printf("Button pressed: Select\n");
			break;

		case CAPOUI_BUTTON_L:
			printf("Button pressed: Left\n");
			break;

		case CAPOUI_BUTTON_D:
			printf("Button pressed: Down\n");
			break;

		case CAPOUI_BUTTON_U:
			printf("Button pressed: Up\n");
			break;

		case CAPOUI_BUTTON_R:
			printf("Button pressed: Right\n");
			break;

		case CAPOUI_BUTTON_NOBUTTON:
			printf("Button pressed: NONE\n");
			break;

		default:
			fprintf(stderr, "ERROR: Unsupported button pressed: %02X\n", data[0]);
	}
}

int USB;

void input_handler(uint8_t peripheral, const uint8_t *data, uint8_t data_length)
{
	switch (peripheral) {
		case CAPOUI_PERIPHERAL_BUTTON:
			handle_buttons(data, data_length);
			break;

		default:
			fprintf(stderr, "ERROR: Unsupported peripheral %02X\n", peripheral);

			printf("Received communicate data: ");
			int i;
			for (i = 0; i < data_length; i++)
			{
				printf("%02X", data[i]);
			}
			printf("\n");
	}
}

void* readroutine(void *ign)
{
	unsigned char buf[256], commbuf[256];
	memset(buf, 0, 256);

	while (1)
	{
		int n = CAPOUI_Comm_ReadIntoQueue(USB);

		if (n < 1)
		{
			perror("read");
			continue;
	        }

		while (CAPOUI_ReadQueue_HasComm())
			CAPOUI_Comm_Handle_Input(input_handler);
	}

	exit(0);

	return NULL;
}

int main(int argc, char **argv)
{
	int i;

	if (argc < 2) {
		fprintf(stderr, "Please provide device name (/dev/tty*)\n");
		return 1;
	}

	USB = CAPOUI_Fd_Open(argv[1]);

	if (USB < 0)
	{
		perror("CAPOUI_Fd_Open");
		return 2;
	}

	char *buf;
	size_t bufsize;
	int len, n;
	char k;

	printf("*****************************\n");
	printf("* Connected to CAPO UI      *\n");
	printf("* Write text to LCD         *\n");
	printf("* or enter /reset to clear  *\n");
	printf("*****************************\n\n");

	pthread_t thr;
	pthread_create(&thr, NULL, readroutine, NULL);

	while (1)
	{
		bufsize = 256;
		buf = NULL;

		len = getline(&buf, &bufsize, stdin);
		if (buf[len-1] == '\n') buf[--len] = 0;

		if (buf[0] == '/') {
			if (strcmp(buf+1, "reset") == 0)
			{
				if (!CAPOUI_LCD_Reset(USB))
					perror("CAPOUI_LCD_Reset");
			}
			// and so on
		}
		else
		{
			if (CAPOUI_LCD_Data(USB, buf, len) != 1)
				perror("CAPOUI_LCD_Data");
		}

		free(buf);
	}
	CAPOUI_Fd_Close(USB);
}
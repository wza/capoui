#include "capoui.h"

#include <stdio.h>

int CAPOUI_Fd_Open(const char *device)
{
	int fd;

	fd = open(device, O_RDWR);

	if (fd < 0)
		return -1;

	struct termios tty;
	memset(&tty, 0, sizeof tty);

	if (tcgetattr(fd, &tty) != 0)
		return -2;

	cfsetospeed (&tty, B115200);
	cfsetispeed (&tty, B115200);

	tty.c_cflag &= ~PARENB;        // Make 8n1
	tty.c_cflag &= ~CSTOPB;
	tty.c_cflag &= ~CSIZE;
	tty.c_cflag |= CS8;
	tty.c_cflag &= ~CRTSCTS;       // no flow control
	tty.c_lflag = 0;          // no signaling chars, no echo, no canonical processing
	tty.c_oflag = 0;                  // no remapping, no delays
	tty.c_cc[VMIN] = 1;                  // read doesn't block
	tty.c_cc[VTIME] = 5;                  // 0.5 seconds read timeout

	tty.c_cflag |= CREAD | CLOCAL;     // turn on READ & ignore ctrl lines
	tty.c_iflag &= ~(IXON | IXOFF | IXANY);// turn off s/w flow ctrl
	tty.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG); // make raw
	tty.c_oflag &= ~OPOST;              // make raw

	tcflush(fd, TCIFLUSH);

	if (tcsetattr(fd, TCSANOW, &tty) != 0)
		return -3;

	return fd;
}

void CAPOUI_Fd_Close(int fd)
{
	if (fd < 0)
		return;

	close(fd);
}

// COMMUNIQUE
uint8_t CAPOUI_Comm_Send(int fd, const uint8_t *comm)
{
	if (fd < 0)
		return 0;

	if (comm == NULL)
		return 0;

	if (comm[0] < 2)
		// no point in sending that short communique
		return 0;

	uint8_t len = comm[0];
	uint8_t sent = 0;
	int result;

	while(len - sent) {
		result = write(fd, comm + sent, len - sent > 5 ? 5 : len-sent);

		if (result < 0)
			return -1;

		sent += result;
	}

	return sent;
}

static uint8_t read_queue[256];
static uint8_t read_queue_used = 0;

void CAPOUI_ReadQueue_Feed(const uint8_t *data, unsigned int length)
{
	memcpy(read_queue + read_queue_used, data, length);
	read_queue_used += length;

	while (CAPOUI_ReadQueue_HasComm() && read_queue[0] < 2)
		// auto-consume too short communiques into oblivion
		CAPOUI_ReadQueue_TakeComm(NULL);
}

uint8_t CAPOUI_ReadQueue_HasComm()
{
	return read_queue_used > 0 && read_queue_used >= read_queue[0];
}

void CAPOUI_ReadQueue_TakeComm(uint8_t *buffer)
{
	if (!CAPOUI_ReadQueue_HasComm()) // check for sure
		return;

	if (read_queue[0] == 0)
		// override length to 1
		read_queue[0] = 1;

	if (buffer != NULL)
		memcpy(buffer, read_queue, read_queue[0]);

	read_queue_used -= read_queue[0];
	
	uint8_t i, by = read_queue[0];

	// remove consumed communique
	for (i = 0; i < read_queue_used; i++)
	{
		read_queue[i] = read_queue[i + by];
	}
}

uint8_t CAPOUI_Comm_ReadIntoQueue(int fd)
{
	uint8_t buf[256];

	int n = read(fd, buf, 255);

	if (n < 1)
		return 0;

	CAPOUI_ReadQueue_Feed(buf, n);

	return n;
}

void CAPOUI_Comm_Handle_Input(void(*handler)(uint8_t, const uint8_t*, uint8_t))
{
	uint8_t comm[256];

	if (!CAPOUI_ReadQueue_HasComm())
		return;

	CAPOUI_ReadQueue_TakeComm(comm);


	if (comm[0] < 2) // too short communique
		return;	

	handler(comm[1], comm + 2, comm[0] - 2);
}

// PERIPHERAL LCD
uint8_t CAPOUI_LCD_Data(int fd, const char *str, unsigned int len)
{
	int sent;

	char *comm = malloc(len + 3);
	if (comm == NULL)
		return 0;

	comm[0] = len + 3;
	comm[1] = CAPOUI_PERIPHERAL_LCD;
	comm[2] = CAPOUI_LCD_DATA;
	strncpy(comm + 3, str, len);

	sent = CAPOUI_Comm_Send(fd, comm);

	free(comm);

	return sent == len + 3;
}

uint8_t CAPOUI_LCD_Goto(int fd, uint8_t x, uint8_t y)
{
	char comm[5];

	if (x == 0 || x > 40 || y > 4 || y == 0)
		return 0;

	comm[0] = 5;
	comm[1] = CAPOUI_PERIPHERAL_LCD;
	comm[2] = CAPOUI_LCD_GOTO;
	comm[3] = x;
	comm[4] = y;

	return CAPOUI_Comm_Send(fd, comm) == 5;
}

uint8_t CAPOUI_LCD_Shift(int fd, uint8_t move, uint8_t dir)
{
	char comm[5];

	if (move != CAPOUI_LCD_SHIFT_CURSOR && move != CAPOUI_LCD_SHIFT_DISPLAY)
		return 0;

	if (dir != CAPOUI_LCD_SHIFT_LEFT && dir != CAPOUI_LCD_SHIFT_RIGHT)
		return 0;

	comm[0] = 5;
	comm[1] = CAPOUI_PERIPHERAL_LCD;
	comm[2] = CAPOUI_LCD_SHIFT;
	comm[3] = move;
	comm[4] = dir;

	return CAPOUI_Comm_Send(fd, comm) == 5;
}

uint8_t CAPOUI_LCD_Reset(int fd)
{
	char comm[3];

	comm[0] = 3;
	comm[1] = CAPOUI_PERIPHERAL_LCD;
	comm[2] = CAPOUI_LCD_RESET;

	return CAPOUI_Comm_Send(fd, comm) == 3;
}

uint8_t CAPOUI_LCD_EntryMode(int fd, uint8_t move, uint8_t dir)
{
	char comm[5];

	if (move != CAPOUI_LCD_ENTRY_MODE_SHIFT_CURSOR && move != CAPOUI_LCD_ENTRY_MODE_SHIFT_DISPLAY)
		return 0;

	if (dir != CAPOUI_LCD_ENTRY_MODE_SHIFT_LEFT && dir != CAPOUI_LCD_ENTRY_MODE_SHIFT_RIGHT)
		return 0;

	comm[0] = 5;
	comm[1] = CAPOUI_PERIPHERAL_LCD;
	comm[2] = CAPOUI_LCD_ENTRY_MODE;
	comm[3] = move;
	comm[4] = dir;

	return CAPOUI_Comm_Send(fd, comm) == 5;
}

uint8_t CAPOUI_LCD_DisplayMode(int fd, uint8_t screen, uint8_t cursor)
{
	char comm[5];

	if (screen != CAPOUI_LCD_DISPLAY_MODE_SCREEN_OFF && screen != CAPOUI_LCD_DISPLAY_MODE_SCREEN_ON)
		return 0;

	if (cursor != CAPOUI_LCD_DISPLAY_MODE_CURSOR_OFF && cursor != CAPOUI_LCD_DISPLAY_MODE_CURSOR_BLINK && cursor != CAPOUI_LCD_DISPLAY_MODE_CURSOR_ON)
		return 0;

	comm[0] = 5;
	comm[1] = CAPOUI_PERIPHERAL_LCD;
	comm[2] = CAPOUI_LCD_DISPLAY_MODE;
	comm[3] = screen;
	comm[4] = cursor;

	return CAPOUI_Comm_Send(fd, comm) == 5;
}

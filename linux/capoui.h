#ifndef __CAPOUI_H
#define __CAPOUI_H

#define _GNU_SOURCE
#include <stdlib.h>
#include <string.h>     // string function definitions
#include <unistd.h>     // UNIX standard function definitions
#include <fcntl.h>      // File control definitions
#include <errno.h>      // Error number definitions
#include <termios.h>    // POSIX terminal control definitions
#include <stdint.h>

#define CAPOUI_PERIPHERAL_BUTTON 0x01
#define CAPOUI_PERIPHERAL_LCD 0x02

#define CAPOUI_LCD_DATA         0x01
#define CAPOUI_LCD_GOTO         0x02
#define CAPOUI_LCD_SHIFT        0x03
#define CAPOUI_LCD_RESET        0x04
#define CAPOUI_LCD_ENTRY_MODE   0x05
#define CAPOUI_LCD_DISPLAY_MODE 0x06

#define CAPOUI_LCD_SHIFT_CURSOR  0x01
#define CAPOUI_LCD_SHIFT_DISPLAY 0x02
#define CAPOUI_LCD_SHIFT_LEFT    0x01
#define CAPOUI_LCD_SHIFT_RIGHT   0x02

#define CAPOUI_LCD_ENTRY_MODE_SHIFT_CURSOR  0x01
#define CAPOUI_LCD_ENTRY_MODE_SHIFT_DISPLAY 0x02
#define CAPOUI_LCD_ENTRY_MODE_SHIFT_LEFT    0x01
#define CAPOUI_LCD_ENTRY_MODE_SHIFT_RIGHT   0x02

#define CAPOUI_LCD_DISPLAY_MODE_SCREEN_OFF 0x00
#define CAPOUI_LCD_DISPLAY_MODE_SCREEN_ON  0x01
#define CAPOUI_LCD_DISPLAY_MODE_CURSOR_OFF 0x00
#define CAPOUI_LCD_DISPLAY_MODE_CURSOR_BLINK 0x01
#define CAPOUI_LCD_DISPLAY_MODE_CURSOR_ON 0x02

#define CAPOUI_BUTTON_NOBUTTON 0
#define CAPOUI_BUTTON_S 1
#define CAPOUI_BUTTON_L 2
#define CAPOUI_BUTTON_D 3
#define CAPOUI_BUTTON_U 4
#define CAPOUI_BUTTON_R 5

// FD
int CAPOUI_Fd_Open(const char *);
void CAPOUI_Fd_Close(int);

// COMMUNIQUE
uint8_t CAPOUI_Comm_Send(int, const uint8_t *);
uint8_t CAPOUI_Comm_ReadIntoQueue(int);
void CAPOUI_Comm_Handle_Input(void(*)(uint8_t, const uint8_t*, uint8_t));

// READ QUEUE
void CAPOUI_ReadQueue_Feed(const uint8_t *, unsigned int);
uint8_t CAPOUI_ReadQueue_HasComm();
void CAPOUI_ReadQueue_TakeComm(uint8_t *);

// PERIPHERAL LCD
uint8_t CAPOUI_LCD_Data(int fd, const char *str, unsigned int len);
uint8_t CAPOUI_LCD_Goto(int fd, uint8_t x, uint8_t y);
uint8_t CAPOUI_LCD_Shift(int fd, uint8_t move, uint8_t dir);
uint8_t CAPOUI_LCD_Reset(int fd);
uint8_t CAPOUI_LCD_EntryMode(int fd, uint8_t move, uint8_t dir);
uint8_t CAPOUI_LCD_DisplayMode(int fd, uint8_t screen, uint8_t cursor);

#endif

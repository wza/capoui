#include "halt.h"

static volatile uint8_t HALT_en = 0;
static char HALT_msg[17];
	
void HALT(const char *text)
{
	strncpy(HALT_msg, text, 16);
	HALT_msg[16] = 0;
	HALT_en = 1;
}

void HALT_Handle(void)
{
	if (!HALT_en)
	{
		return;
	}
	
	while(1) {
		LCD_Clear();
		LCD_GoTo(0,0);
		LCD_WriteText("HALT:");
		LCD_GoTo(0, 1);
		LCD_WriteText(HALT_msg);
		HAL_Delay(1000);
	}
}

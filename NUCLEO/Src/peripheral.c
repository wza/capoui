#include "peripheral.h"

static uint8_t comm_output_buffer[256];

void PERIPHERAL_Initialize(void)
{
	PERIPHERAL_LCD_Initialize();
  PERIPHERAL_BUTTON_Initialize();
}

void PERIPHERAL_Handle_IO(void(*input_handler)(uint8_t*), uint8_t(*output_handler)(uint8_t*))
{
	PERIPHERAL_BUTTON_Handle(input_handler);
	
	// Add other IN peripherals here

	while (output_handler(comm_output_buffer))
	{
		if (comm_output_buffer[0] < 2)
		{
			// dummy message
			continue;
		}
		
		switch (comm_output_buffer[1])
		{
			case PERIPHERAL_LCD:
				PERIPHERAL_LCD_Handle(comm_output_buffer);
				break;
			
			// Add other OUT peripherals here
			
			default:
				// unsupported peripheral
			  break;
		}
	}
}

#include "peripheral_button.h"

static ADC_HandleTypeDef BUTTON_ADC_Handle;
static uint32_t BUTTON_read_prev = 0;
static uint32_t BUTTON_read_time = 0;
static uint8_t BUTTON_val_prev = 0;

void PERIPHERAL_BUTTON_Initialize(void)
{
	__GPIOA_CLK_ENABLE();
	__ADC1_CLK_ENABLE();
	
	GPIO_InitTypeDef gpioInit;

	gpioInit.Pin = GPIO_PIN_0;
	gpioInit.Mode = GPIO_MODE_ANALOG;
	gpioInit.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(GPIOA, &gpioInit);
 
	BUTTON_ADC_Handle.Instance = ADC1;
	BUTTON_ADC_Handle.Init.ClockPrescaler = ADC_CLOCKPRESCALER_PCLK_DIV2;
	BUTTON_ADC_Handle.Init.Resolution = ADC_RESOLUTION12b;
	BUTTON_ADC_Handle.Init.ContinuousConvMode = ENABLE;
	BUTTON_ADC_Handle.Init.DiscontinuousConvMode = DISABLE;
	BUTTON_ADC_Handle.Init.DataAlign = ADC_DATAALIGN_RIGHT;
	BUTTON_ADC_Handle.Init.DMAContinuousRequests = ENABLE;
	BUTTON_ADC_Handle.Init.EOCSelection = EOC_SINGLE_CONV;
	HAL_ADC_Init(&BUTTON_ADC_Handle);
	
	ADC_ChannelConfTypeDef sConfig;
	sConfig.Channel = ADC_CHANNEL_0; 
	HAL_ADC_ConfigChannel(&BUTTON_ADC_Handle, &sConfig);
}

static uint8_t PERIPHERAL_BUTTON_ConvertToPeripheral(uint32_t in)
{
	if (in < 200) return 5;
	if (in < 1000) return 4;
  if (in < 2000) return 3;
	if (in < 3000) return 2;
	if (in < 4000) return 1;
	return 0;
}

static uint32_t PERIPHERAL_BUTTON_Read(void)
{
	if (HAL_GetTick() < BUTTON_read_time + 50) {
		return BUTTON_read_prev;
	}
	
	BUTTON_read_time = HAL_GetTick();
	
	HAL_ADC_Start(&BUTTON_ADC_Handle);

	if (HAL_ADC_PollForConversion(&BUTTON_ADC_Handle, 1000000) == HAL_OK)
	{
		return HAL_ADC_GetValue(&BUTTON_ADC_Handle);
	}
	
	return 0xFFFF;
}

uint8_t PERIPHERAL_BUTTON_Handle(void(*input_handler)(uint8_t*))
{
	uint32_t read = PERIPHERAL_BUTTON_Read();
	uint8_t val;
	uint8_t comm[3];
		
	if (read == BUTTON_read_prev)
	{
	  return 0;
	}
	
	BUTTON_read_prev = read;
	
	val = PERIPHERAL_BUTTON_ConvertToPeripheral(read);

	if (val == BUTTON_val_prev)
	{
		return 0;
	}

	BUTTON_val_prev = val;

	comm[0] = 0x03;
	comm[1] = PERIPHERAL_BUTTON;
	comm[2] = val;
	
	input_handler(comm);
	
	return 1;
}

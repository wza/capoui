#include "protocol.h"

void PROTOCOL_Input(uint8_t *comm)
{
	if (comm[0] < 2)
	{
		return;
	}
	
	CDC_Send(comm, comm[0]);
}

uint8_t PROTOCOL_buffer[256];
uint8_t PROTOCOL_buffer_end = 0;

uint8_t PROTOCOL_Output(uint8_t *comm)
{
	uint8_t avail;

	while (1)
	{
		avail = CDC_Available();

		if (avail == 0)
			break;
		
		while (avail)
		{
			PROTOCOL_buffer[PROTOCOL_buffer_end++] = CDC_Consume();
			
			if (PROTOCOL_buffer_end >= PROTOCOL_buffer[0])
			{
				memcpy(comm, PROTOCOL_buffer, PROTOCOL_buffer[0]);
				PROTOCOL_buffer_end = 0;
				return 1;
			}

			avail--;
		}
	}
	
	return 0;
}

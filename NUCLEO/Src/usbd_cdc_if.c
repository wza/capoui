/**
  ******************************************************************************
  * @file    usbd_cdc_if_template.c
  * @author  MCD Application Team
  * @version V2.2.0
  * @date    13-June-2014
  * @brief   Generic media access Layer.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2014 STMicroelectronics</center></h2>
  *
  * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://www.st.com/software_license_agreement_liberty_v2
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */ 

/* Includes ------------------------------------------------------------------*/
#include "usbd_cdc_if.h"

/** @addtogroup STM32_USB_DEVICE_LIBRARY
  * @{
  */


/** @defgroup USBD_CDC 
  * @brief usbd core module
  * @{
  */ 

/** @defgroup USBD_CDC_Private_TypesDefinitions
  * @{
  */ 
/**
  * @}
  */ 


/** @defgroup USBD_CDC_Private_Defines
  * @{
  */ 
/**
  * @}
  */ 


/** @defgroup USBD_CDC_Private_Macros
  * @{
  */ 

/**
  * @}
  */ 


/** @defgroup USBD_CDC_Private_FunctionPrototypes
  * @{
  */
	
extern USBD_HandleTypeDef USBD_Device;

static int8_t CDC_Init     (void);
static int8_t CDC_DeInit   (void);
static int8_t CDC_Control  (uint8_t cmd, uint8_t* pbuf, uint16_t length);
static int8_t CDC_Receive  (uint8_t* pbuf, uint32_t *Len);

//void usb_tim_init(void);


USBD_CDC_ItfTypeDef USBD_CDC_fops = 
{
  CDC_Init,
  CDC_DeInit,
  CDC_Control,
  CDC_Receive
};

USBD_CDC_LineCodingTypeDef linecoding =
  {
    115200, /* baud rate*/
    0x00,   /* stop bits-1*/
    0x00,   /* parity - none*/
    0x08    /* nb. of bits 8*/
  };
	
#define CDC_BUFFER_SIZE 256
uint8_t *CDC_buffer_RX;
	
uint8_t CDC_buffer[CDC_BUFFER_SIZE];
volatile uint16_t CDC_buffer_start = 0;
volatile uint16_t CDC_buffer_end = 0;

uint8_t CDC_initialized = 0;
	
/* Private functions ---------------------------------------------------------*/

/**
  * @brief  TEMPLATE_Init
  *         Initializes the CDC media low layer
  * @param  None
  * @retval Result of the opeartion: USBD_OK if all operations are OK else USBD_FAIL
  */
static int8_t CDC_Init(void)
{
	CDC_buffer_RX = (uint8_t*)malloc(256);
	
	if (CDC_buffer_RX == NULL) HALT("CDC_Init fail");
	
	USBD_CDC_SetRxBuffer(&USBD_Device, CDC_buffer_RX);
	CDC_initialized = 1;

  return (0);
}

/**
  * @brief  TEMPLATE_DeInit
  *         DeInitializes the CDC media low layer
  * @param  None
  * @retval Result of the opeartion: USBD_OK if all operations are OK else USBD_FAIL
  */
static int8_t CDC_DeInit(void)
{
	free(CDC_buffer_RX);
	CDC_buffer_RX = NULL;
  /*
     Add your deinitialization code here 
  */  
  return (0);
}


/**
  * @brief  TEMPLATE_Control
  *         Manage the CDC class requests
  * @param  Cmd: Command code            
  * @param  Buf: Buffer containing command data (request parameters)
  * @param  Len: Number of data to be sent (in bytes)
  * @retval Result of the opeartion: USBD_OK if all operations are OK else USBD_FAIL
  */
static int8_t CDC_Control(uint8_t cmd, uint8_t* pbuf, uint16_t length)
{ 
  switch (cmd)
  {
  case CDC_SEND_ENCAPSULATED_COMMAND:
    /* Add your code here */
    break;

  case CDC_GET_ENCAPSULATED_RESPONSE:
    /* Add your code here */
    break;

  case CDC_SET_COMM_FEATURE:
    /* Add your code here */
    break;

  case CDC_GET_COMM_FEATURE:
    /* Add your code here */
    break;

  case CDC_CLEAR_COMM_FEATURE:
    /* Add your code here */
    break;

  case CDC_SET_LINE_CODING:
    linecoding.bitrate    = (uint32_t)(pbuf[0] | (pbuf[1] << 8) |\
                            (pbuf[2] << 16) | (pbuf[3] << 24));
    linecoding.format     = pbuf[4];
    linecoding.paritytype = pbuf[5];
    linecoding.datatype   = pbuf[6];
    
    /* Add your code here */
    break;

  case CDC_GET_LINE_CODING:
    pbuf[0] = (uint8_t)(linecoding.bitrate);
    pbuf[1] = (uint8_t)(linecoding.bitrate >> 8);
    pbuf[2] = (uint8_t)(linecoding.bitrate >> 16);
    pbuf[3] = (uint8_t)(linecoding.bitrate >> 24);
    pbuf[4] = linecoding.format;
    pbuf[5] = linecoding.paritytype;
    pbuf[6] = linecoding.datatype;     
    
    /* Add your code here */
    break;

  case CDC_SET_CONTROL_LINE_STATE:
    /* Add your code here */
    break;

  case CDC_SEND_BREAK:
     /* Add your code here */
    break;    
    
  default:
    break;
  }

  return (0);
}



/**
  * @brief  TEMPLATE_DataRx
  *         Data received over USB OUT endpoint are sent over CDC interface 
  *         through this function.
  *           
  *         @note
  *         This function will block any OUT packet reception on USB endpoint 
  *         untill exiting this function. If you exit this function before transfer
  *         is complete on CDC interface (ie. using DMA controller) it will result 
  *         in receiving more data while previous ones are still not sent.
  *                 
  * @param  Buf: Buffer of data to be received
  * @param  Len: Number of data received (in bytes)
  * @retval Result of the opeartion: USBD_OK if all operations are OK else USBD_FAIL
  */


static int8_t CDC_Receive (uint8_t* Buf, uint32_t *Len)
{
	uint32_t length = *Len, i;
	uint16_t pos;
	uint8_t data;
	
	if (CDC_buffer_RX == NULL) HALT("CDC_Receive fail");
	
	for (i=0; i < length; i++)
	{
		data = Buf[i];
		pos = CDC_buffer_end + 1;
		
		if (pos == CDC_BUFFER_SIZE)
			pos = 0;
		
		if (pos == CDC_buffer_start)
			break;

		CDC_buffer[CDC_buffer_end] = data;
		
		CDC_buffer_end = pos;
	}

	USBD_CDC_ReceivePacket(&USBD_Device);
	
  return USBD_OK;
}

void CDC_Send(uint8_t *data, uint16_t len)
{
	
	while(((USBD_CDC_HandleTypeDef*)USBD_Device.pClassData)->TxState == 1);	
		
	USBD_CDC_SetTxBuffer(&USBD_Device, data, len);
		
	while (USBD_CDC_TransmitPacket(&USBD_Device) != USBD_OK);

	while(((USBD_CDC_HandleTypeDef*)USBD_Device.pClassData)->TxState == 1);
}

uint8_t CDC_IsInitialized(void)
{
	return CDC_initialized;
}

uint8_t CDC_Available(void)
{
	int16_t x = CDC_buffer_end - CDC_buffer_start;
	return (uint8_t) (x + (x < 0 ? CDC_BUFFER_SIZE : 0));
}

uint8_t CDC_Consume(void)
{
	if (!CDC_Available()) return 0;

	uint8_t data = CDC_buffer[CDC_buffer_start];
	
	CDC_buffer_start++;
	
	if (CDC_buffer_start == CDC_BUFFER_SIZE)
		CDC_buffer_start = 0;
	
	return data;
}

/**
  * @}
  */ 

/**
  * @}
  */ 

/**
  * @}
  */ 

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/


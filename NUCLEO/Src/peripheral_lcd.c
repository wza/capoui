#include "peripheral_lcd.h"

#define PERIPHERAL_LCD_COMMAND_DATA         0x01
#define PERIPHERAL_LCD_COMMAND_GOTO         0x02
#define PERIPHERAL_LCD_COMMAND_SHIFT        0x03
#define PERIPHERAL_LCD_COMMAND_RESET        0x04
#define PERIPHERAL_LCD_COMMAND_ENTRY_MODE   0x05
#define PERIPHERAL_LCD_COMMAND_DISPLAY_MODE 0x06

#define PERIPHERAL_LCD_COMMAND_SHIFT_CURSOR  0x01
#define PERIPHERAL_LCD_COMMAND_SHIFT_DISPLAY 0x02
#define PERIPHERAL_LCD_COMMAND_SHIFT_LEFT    0x01
#define PERIPHERAL_LCD_COMMAND_SHIFT_RIGHT   0x02

#define PERIPHERAL_LCD_COMMAND_ENTRY_MODE_SHIFT_CURSOR  0x01
#define PERIPHERAL_LCD_COMMAND_ENTRY_MODE_SHIFT_DISPLAY 0x02
#define PERIPHERAL_LCD_COMMAND_ENTRY_MODE_SHIFT_LEFT    0x01
#define PERIPHERAL_LCD_COMMAND_ENTRY_MODE_SHIFT_RIGHT   0x02

#define PERIPHERAL_LCD_COMMAND_DISPLAY_MODE_SCREEN_OFF 0x00
#define PERIPHERAL_LCD_COMMAND_DISPLAY_MODE_SCREEN_ON  0x01
#define PERIPHERAL_LCD_COMMAND_DISPLAY_MODE_CURSOR_OFF 0x00
#define PERIPHERAL_LCD_COMMAND_DISPLAY_MODE_CURSOR_BLINK 0x01
#define PERIPHERAL_LCD_COMMAND_DISPLAY_MODE_CURSOR_ON 0x02

void PERIPHERAL_LCD_Initialize(void)
{
	LCD_Initialize();
}

void PERIPHERAL_LCD_Handle(uint8_t *comm)
{
	if (comm[0] < 3)
	{
		// message too short
		return;
	}
	
	switch (comm[2])
	{
		case PERIPHERAL_LCD_COMMAND_DATA:
			{
				int i;

				for (i = 3; i < comm[0]; i++)
				{
					LCD_WriteData(comm[i]);
				}
			}
			break;
			
		case PERIPHERAL_LCD_COMMAND_GOTO:
			{
				uint8_t x, y;

				if (comm[0] < 5)
				{
					// message to short
					break;
				}
				
				y = comm[3] - 1;
				x = comm[4] - 1;
				
				if (y > 3 || x > 39) {
					// wrong coordinates
					break;
				}

				LCD_GoTo(x, y);
			}
			break;
			
		case PERIPHERAL_LCD_COMMAND_SHIFT:
			{
				uint8_t move, dir, command;

				if (comm[0] < 5)
				{
					// message to short
					break;
				}
				
				move = comm[3];
				dir = comm[4];
				
				if (move != PERIPHERAL_LCD_COMMAND_SHIFT_CURSOR && move != PERIPHERAL_LCD_COMMAND_SHIFT_DISPLAY)
				{
					// invalid parameter
					break;
				}
				
				if (dir != PERIPHERAL_LCD_COMMAND_ENTRY_MODE_SHIFT_LEFT && dir != PERIPHERAL_LCD_COMMAND_SHIFT_RIGHT)
				{
					// invalid parameter
					break;
				}
				
				command = HD44780_DISPLAY_CURSOR_SHIFT;
				
				command |= move == PERIPHERAL_LCD_COMMAND_SHIFT_CURSOR ? HD44780_SHIFT_CURSOR : HD44780_SHIFT_DISPLAY;
				command |= dir == PERIPHERAL_LCD_COMMAND_ENTRY_MODE_SHIFT_LEFT ? HD44780_SHIFT_LEFT : HD44780_SHIFT_RIGHT;
				
				LCD_WriteCommand(command);
			}
			break;
			
		case PERIPHERAL_LCD_COMMAND_RESET:
			LCD_Clear();
			break;
		
		case PERIPHERAL_LCD_COMMAND_ENTRY_MODE:
			{
				uint8_t move, dir, command;

				if (comm[0] < 5)
				{
					// message to short
					break;
				}
				
				move = comm[3];
				dir = comm[4];
				
				if (move != PERIPHERAL_LCD_COMMAND_ENTRY_MODE_SHIFT_CURSOR && move != PERIPHERAL_LCD_COMMAND_ENTRY_MODE_SHIFT_DISPLAY)
				{
					// invalid parameter
					break;
				}

				if (dir != PERIPHERAL_LCD_COMMAND_ENTRY_MODE_SHIFT_LEFT && dir != PERIPHERAL_LCD_COMMAND_ENTRY_MODE_SHIFT_RIGHT)
				{
					// invalid parameter
					break;
				}
				
				command = HD44780_ENTRY_MODE;
				
				command |= move == PERIPHERAL_LCD_COMMAND_ENTRY_MODE_SHIFT_CURSOR ? HD44780_EM_SHIFT_CURSOR : HD44780_EM_SHIFT_DISPLAY;
				command |= dir == PERIPHERAL_LCD_COMMAND_ENTRY_MODE_SHIFT_LEFT ? HD44780_EM_DECREMENT : HD44780_EM_INCREMENT;
				
				LCD_WriteCommand(command);
			}
			break;

		case PERIPHERAL_LCD_COMMAND_DISPLAY_MODE:
			{
				uint8_t screen, cursor, command;

				if (comm[0] < 5)
				{
					// message to short
					break;
				}
				
				screen = comm[3];
				cursor = comm[4];
				
				if (screen != PERIPHERAL_LCD_COMMAND_DISPLAY_MODE_SCREEN_OFF && screen != PERIPHERAL_LCD_COMMAND_DISPLAY_MODE_SCREEN_ON)
				{
				  // invalid parameter
					break;
				}
				
				if (cursor != PERIPHERAL_LCD_COMMAND_DISPLAY_MODE_CURSOR_OFF && cursor != PERIPHERAL_LCD_COMMAND_DISPLAY_MODE_CURSOR_BLINK && cursor != PERIPHERAL_LCD_COMMAND_DISPLAY_MODE_CURSOR_ON)
				{
				  // invalid parameter
					break;
				}
				
				command = HD44780_DISPLAY_ONOFF;
				command |= screen == PERIPHERAL_LCD_COMMAND_DISPLAY_MODE_SCREEN_ON ? HD44780_DISPLAY_ON : HD44780_DISPLAY_OFF;
				command |= cursor == PERIPHERAL_LCD_COMMAND_DISPLAY_MODE_CURSOR_OFF ? HD44780_CURSOR_OFF : (HD44780_CURSOR_ON | (cursor == PERIPHERAL_LCD_COMMAND_DISPLAY_MODE_CURSOR_BLINK ? HD44780_CURSOR_BLINK : HD44780_CURSOR_NOBLINK));
				
				LCD_WriteCommand(command);
			}
			break;

		}
}

#ifndef __PERIPHERAL_H
#define __PERIPHERAL_H

#include "peripheral_button.h"
#include "peripheral_lcd.h"

void PERIPHERAL_Initialize(void);
void PERIPHERAL_Handle_IO(void(*input_handler)(uint8_t*), uint8_t(*output_handler)(uint8_t*));

#endif

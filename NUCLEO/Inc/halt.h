#ifndef __HALT_H
#define __HALT_H

#include <string.h>

#include "HD44780.h"

void HALT(const char *text);
void HALT_Handle(void);

#endif

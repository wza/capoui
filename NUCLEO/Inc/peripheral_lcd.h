#ifndef __PERIPHERAL_LCD_H
#define __PERIPHERAL_LCD_H

#include "HD44780.h"

#define PERIPHERAL_LCD 0x02

void PERIPHERAL_LCD_Initialize(void);
void PERIPHERAL_LCD_Handle(uint8_t*);

#endif

#ifndef __PROTOCOL_H
#define __PROTOCOL_H

#include "usbd_cdc_if.h"

void PROTOCOL_Input(uint8_t *comm);
uint8_t PROTOCOL_Output(uint8_t *comm);

#endif

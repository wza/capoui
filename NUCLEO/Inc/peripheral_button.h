#ifndef __PERIPHERAL_BUTTON_H
#define __PERIPHERAL_BUTTON_H

#include "stm32l0xx_hal.h"

#define PERIPHERAL_BUTTON 0x01

void PERIPHERAL_BUTTON_Initialize(void);
uint8_t PERIPHERAL_BUTTON_Handle(void(*input_handler)(uint8_t*));

#endif

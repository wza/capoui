#include "stm32l0xx_hal.h"

//backlight control na PB6

#define LCD_RS_PORT 	GPIOA
#define LCD_RS			GPIO_PIN_9

#define LCD_E_PORT		GPIOC
#define LCD_E			GPIO_PIN_7

#define LCD_DB4_PORT	GPIOB
#define LCD_DB4			GPIO_PIN_5

#define LCD_DB5_PORT	GPIOB
#define LCD_DB5			GPIO_PIN_4

#define LCD_DB6_PORT	GPIOB
#define LCD_DB6			GPIO_PIN_10

#define LCD_DB7_PORT	GPIOA
#define LCD_DB7			GPIO_PIN_8



#define HD44780_CLEAR                0x01

#define HD44780_HOME                 0x02

#define HD44780_ENTRY_MODE           0x04
#define HD44780_EM_SHIFT_CURSOR      0x00
#define HD44780_EM_SHIFT_DISPLAY     0x01
#define HD44780_EM_DECREMENT         0x00
#define HD44780_EM_INCREMENT         0x02

#define HD44780_DISPLAY_ONOFF	       0x08
#define HD44780_DISPLAY_OFF          0x00
#define HD44780_DISPLAY_ON           0x04
#define HD44780_CURSOR_OFF           0x00
#define HD44780_CURSOR_ON            0x02
#define HD44780_CURSOR_NOBLINK       0x00
#define HD44780_CURSOR_BLINK         0x01

#define HD44780_DISPLAY_CURSOR_SHIFT 0x10
#define HD44780_SHIFT_CURSOR         0x00
#define HD44780_SHIFT_DISPLAY        0x08
#define HD44780_SHIFT_LEFT           0x00
#define HD44780_SHIFT_RIGHT          0x04

#define HD44780_FUNCTION_SET         0x20
#define HD44780_FONT5x7				       0x00
#define HD44780_FONT5x10             0x04
#define HD44780_ONE_LINE             0x00
#define HD44780_TWO_LINE             0x08
#define HD44780_4_BIT                0x00
#define HD44780_8_BIT                0x10

#define HD44780_CGRAM_SET            0x40

#define HD44780_DDRAM_SET            0x80

void LCD_WriteCommand(uint8_t);
void LCD_WriteData(uint8_t);
void LCD_WriteText(const char *);
void LCD_GoTo(uint8_t, uint8_t);
void LCD_Clear(void);
void LCD_Home(void);
void LCD_ShiftLeft(void);
void LCD_ShiftRight(void);
void LCD_Initialize(void);
void LCD_WriteHex(const char);

#include "HD44780.h"

void LCD_WriteNibble(uint8_t nibble)
{
	HAL_GPIO_WritePin(LCD_E_PORT, LCD_E, GPIO_PIN_SET);
	HAL_GPIO_WritePin(LCD_DB4_PORT, LCD_DB4, (nibble & 0x01) ? GPIO_PIN_SET : GPIO_PIN_RESET);
	HAL_GPIO_WritePin(LCD_DB5_PORT, LCD_DB5, (nibble & 0x02) ? GPIO_PIN_SET : GPIO_PIN_RESET);
	HAL_GPIO_WritePin(LCD_DB6_PORT, LCD_DB6, (nibble & 0x04) ? GPIO_PIN_SET : GPIO_PIN_RESET);
	HAL_GPIO_WritePin(LCD_DB7_PORT, LCD_DB7, (nibble & 0x08) ? GPIO_PIN_SET : GPIO_PIN_RESET);
	HAL_GPIO_WritePin(LCD_E_PORT, LCD_E, GPIO_PIN_RESET);
}

void LCD_WriteByte(uint8_t dataToWrite)
{	
	LCD_WriteNibble(dataToWrite >> 4);
  LCD_WriteNibble(dataToWrite & 0x0F);

	volatile uint16_t x = 0;
	while(++x < 100);
}

void LCD_WriteCommand(uint8_t command)
{
	HAL_GPIO_WritePin(LCD_RS_PORT, LCD_RS, GPIO_PIN_RESET);
  LCD_WriteByte(command);
}

void LCD_WriteData(uint8_t data)
{
	HAL_GPIO_WritePin(LCD_RS_PORT, LCD_RS, GPIO_PIN_SET);
  LCD_WriteByte(data);
}

void LCD_WriteText(const char *text)
{
	while (*text)
	{
		LCD_WriteData((uint8_t)*text++);
	}
}

void LCD_GoTo(uint8_t x, uint8_t y)
{
  LCD_WriteCommand(HD44780_DDRAM_SET | (x + (0x40 * y)));
}

void LCD_Clear(void)
{
	LCD_WriteCommand(HD44780_CLEAR);
	HAL_Delay(3);
}

void LCD_Home(void)
{
	LCD_WriteCommand(HD44780_HOME);
	HAL_Delay(2);
}

void LCD_ShiftLeft(void)
{
	LCD_WriteCommand(HD44780_DISPLAY_CURSOR_SHIFT | HD44780_SHIFT_LEFT | HD44780_SHIFT_DISPLAY);
}

void LCD_ShiftRight(void)
{
	LCD_WriteCommand(HD44780_DISPLAY_CURSOR_SHIFT | HD44780_SHIFT_RIGHT | HD44780_SHIFT_DISPLAY);
}

void LCD_Initialize(void)
{
	unsigned char i;
	
	__GPIOA_CLK_ENABLE();
	__GPIOB_CLK_ENABLE();
	__GPIOC_CLK_ENABLE();

	{
		GPIO_InitTypeDef GPIO_InitStruct;
		GPIO_InitStruct.Pin = LCD_DB4;
		GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
		GPIO_InitStruct.Pull = GPIO_PULLUP;
		GPIO_InitStruct.Speed = GPIO_SPEED_HIGH;
		HAL_GPIO_Init(LCD_DB4_PORT, &GPIO_InitStruct);
	}
	
	{
		GPIO_InitTypeDef GPIO_InitStruct;
		GPIO_InitStruct.Pin = LCD_DB5;
		GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
		GPIO_InitStruct.Pull = GPIO_PULLUP;
		GPIO_InitStruct.Speed = GPIO_SPEED_HIGH;
		HAL_GPIO_Init(LCD_DB5_PORT, &GPIO_InitStruct);
	}
		
	{
		GPIO_InitTypeDef GPIO_InitStruct;
		GPIO_InitStruct.Pin = LCD_DB6;
		GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
		GPIO_InitStruct.Pull = GPIO_PULLUP;
		GPIO_InitStruct.Speed = GPIO_SPEED_HIGH;
		HAL_GPIO_Init(LCD_DB6_PORT, &GPIO_InitStruct);
	}
		
	{
		GPIO_InitTypeDef GPIO_InitStruct;
		GPIO_InitStruct.Pin = LCD_DB7;
		GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
		GPIO_InitStruct.Pull = GPIO_PULLUP;
		GPIO_InitStruct.Speed = GPIO_SPEED_HIGH;
		HAL_GPIO_Init(LCD_DB7_PORT, &GPIO_InitStruct);
	}
	
	{
		GPIO_InitTypeDef GPIO_InitStruct;
		GPIO_InitStruct.Pin = LCD_E;
		GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
		GPIO_InitStruct.Pull = GPIO_PULLUP;
		GPIO_InitStruct.Speed = GPIO_SPEED_HIGH;
		HAL_GPIO_Init(LCD_E_PORT, &GPIO_InitStruct);
	}

	{
		GPIO_InitTypeDef GPIO_InitStruct;
		GPIO_InitStruct.Pin =  LCD_RS;
		GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
		GPIO_InitStruct.Pull = GPIO_PULLUP;
		GPIO_InitStruct.Speed = GPIO_SPEED_HIGH;
		HAL_GPIO_Init(LCD_RS_PORT, &GPIO_InitStruct);
	}
	
	HAL_Delay(15); 

	HAL_GPIO_WritePin(LCD_RS_PORT, LCD_RS, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(LCD_E_PORT, LCD_E, GPIO_PIN_RESET);
	
	for(i = 0; i < 3; i++)
	{
		LCD_WriteNibble(0x03);
		
		HAL_Delay(5); 
  }

	LCD_WriteNibble(0x02); 

	HAL_Delay(1); 

	LCD_WriteCommand(HD44780_FUNCTION_SET | HD44780_FONT5x7 | HD44780_TWO_LINE | HD44780_4_BIT); 
	LCD_WriteCommand(HD44780_DISPLAY_ONOFF | HD44780_DISPLAY_OFF); 
	LCD_WriteCommand(HD44780_CLEAR);
	
	HAL_Delay(2);
	
	LCD_WriteCommand(HD44780_ENTRY_MODE | HD44780_EM_SHIFT_CURSOR | HD44780_EM_INCREMENT);
	LCD_WriteCommand(HD44780_DISPLAY_ONOFF | HD44780_DISPLAY_ON | HD44780_CURSOR_OFF | HD44780_CURSOR_NOBLINK);
}

void LCD_WriteHex(const char c)
{
	uint8_t a, b;
	
	a = (c & 0xF0) >> 4;
	b = (c & 0x0F);
	
	LCD_WriteData(a + (a > 9 ? 'A'-10 : '0'));
	LCD_WriteData(b + (b > 9 ? 'A'-10 : '0'));
}
